import {
  getPanelsData,
  getPanelData,
  getEntityPanel,
  getEntitiesData,
  getEntities,
  getSubpanels,
  getPanelSubpanels,
} from "../utils";
import results1 from "../src/fixtures/api/panels/results-1.json";
import results2 from "../src/fixtures/api/panels/results-2.json";
import apiPanelData from "../src/fixtures/api/panels/25/v2.4.json";
import entities from "../src/fixtures/gatsby-node-entities.json";
import mockCache from "../src/fixtures/data/panels/25/v2.4.json";

import fs from "fs";
import axios from "axios";
jest.mock("fs");
jest.mock("axios");

beforeEach(() => {
  axios.get.mockReset();
  fs.existsSync.mockReset();
  fs.mkdirSync.mockReset();
  fs.writeFileSync.mockReset();
});

const reporter = { info: jest.fn() };

describe("getPanelsData()", () => {
  test("Unsuccessful request", async () => {
    axios.get.mockImplementation(async () => {
      throw new Error("Request failed with status code 500");
    });

    await expect(getPanelsData(reporter)).rejects.toThrow(
      "Request failed with status code 500"
    );
  });

  test("Empty results", async () => {
    axios.get.mockImplementation(async () => ({
      data: {
        count: 0,
        next: null,
        previous: null,
        results: [],
      },
    }));

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual([]);
  });

  test("Results without next page", async () => {
    axios.get.mockImplementation(async () => ({
      data: {
        count: 3,
        next: null,
        previous: null,
        results: results1,
      },
    }));

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual(
      [...results1]
        .map(panelSummary => ({
          id: panelSummary.id,
          name: panelSummary.name,
          version: panelSummary.version,
          signed_off: panelSummary.signed_off,
          relevant_disorders: panelSummary.relevant_disorders,
          types: panelSummary.types.map(type => ({ name: type.name })),
          stats: {
            number_of_genes: panelSummary.stats.number_of_genes,
          },
        }))
        .sort((p1, p2) =>
          p1.name.toLowerCase().localeCompare(p2.name.toLowerCase())
        )
    );
  });

  test("Results with next page", async () => {
    axios.get.mockImplementation(async url =>
      url === "/next-page"
        ? {
            data: {
              count: 3,
              next: null,
              previous: "/first-page",
              results: results2,
            },
          }
        : {
            data: {
              count: 3,
              next: "/next-page",
              previous: null,
              results: results1,
            },
          }
    );

    const panelsData = await getPanelsData(reporter);
    expect(panelsData).toEqual(
      [...results1, ...results2]
        .map(panelSummary => ({
          id: panelSummary.id,
          name: panelSummary.name,
          version: panelSummary.version,
          signed_off: panelSummary.signed_off,
          relevant_disorders: panelSummary.relevant_disorders,
          types: panelSummary.types.map(type => ({ name: type.name })),
          stats: {
            number_of_genes: panelSummary.stats.number_of_genes,
          },
        }))
        .sort((p1, p2) =>
          p1.name.toLowerCase().localeCompare(p2.name.toLowerCase())
        )
    );
  });
});

describe("getPanelData()", () => {
  test("Unsuccessful request", async () => {
    fs.existsSync.mockImplementation(() => false);
    axios.get.mockImplementation(async () => {
      throw new Error("Request failed with status code 500");
    });

    await expect(getPanelsData(reporter)).rejects.toThrow(
      "Request failed with status code 500"
    );
  });

  test("Request with cache", async () => {
    jest.mock(`../data/panels/25/v2.4.json`, () => mockCache, {
      virtual: true,
    });

    const id = 25;
    const version = "2.4";

    fs.existsSync.mockImplementation(() => true);

    const panelData = await getPanelData(id, version);
    expect(panelData).toEqual(mockCache);

    expect(fs.existsSync).toHaveBeenCalledTimes(1);
    expect(fs.existsSync).toHaveBeenCalledWith(
      `./data/panels/${id}/v${version}.json`
    );
  });

  test("Request without cache", async () => {
    const id = 399;
    const version = "2.4";

    fs.existsSync.mockImplementation(() => false);
    axios.get.mockImplementation(async () => ({ data: apiPanelData }));

    const panelData = await getPanelData(id, version);
    expect(panelData).toEqual(mockCache);

    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(
      `https://panelapp.genomicsengland.co.uk/api/v1/panels/${id}/?version=${version}`
    );

    expect(fs.mkdirSync).toHaveBeenCalledTimes(1);
    expect(fs.mkdirSync).toHaveBeenCalledWith(`./data/panels/${id}`, {
      recursive: true,
    });
    expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
    expect(fs.writeFileSync).toHaveBeenCalledWith(
      `./data/panels/${id}/v${version}.json`,
      JSON.stringify(panelData)
    );
  });
});

test("getEntityPanel()", () => {
  const panelData = mockCache;
  const panelEntity = mockCache.entities[0];

  expect(getEntityPanel(panelData, panelEntity)).toEqual({
    id: panelData.panel.id,
    name: panelData.panel.name,
    version: panelData.panel.version,
    relevant_disorders: panelData.panel.relevant_disorders,
    entity: {
      confidence_level: panelEntity.confidence_level,
      mode_of_inheritance: panelEntity.mode_of_inheritance,
      phenotypes: panelEntity.phenotypes,
    },
  });
});

test("getEntitiesData()", () => {
  const entitiesData = getEntitiesData(entities);
  expect(entitiesData).toEqual({
    entities: [
      {
        entity_name: "AAAS",
        entity_type: "gene",
      },
      {
        entity_name: "AARS",
        entity_type: "gene",
      },
    ],
  });
});

describe("getSubpanels()", () => {
  test("with a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        id: 486,
        name: "Paediatric disorders",
        version: "14.43",
      },
    ];
    const subpanels = await getSubpanels(panelsData);
    const subpanelNames = Object.keys(subpanels);

    expect(subpanelNames.length).toBe(11);
    expect(subpanels["DDG2P"].superpanels).toEqual(["Paediatric disorders"]);
  });

  test("without a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        id: 285,
        name: "Intellectual disability",
        version: "3.2",
      },
    ];

    const subpanels = await getSubpanels(panelsData);
    expect(subpanels).toEqual({});
  });
});

describe("getEntities()", () => {
  test("with a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        id: 486,
        name: "Paediatric disorders",
        version: "14.43",
      },
    ];
    const entities = await getEntities(panelsData);
    expect(entities).toEqual({});
  });

  test("without a superpanel", async () => {
    fs.existsSync.mockImplementation(() => true);
    const panelsData = [
      {
        // Amber panel for AAAS
        id: 847,
        name: "Childhood onset dystonia or chorea or related movement disorder",
        version: "1.58",
      },
      {
        // Green panel for AAAS
        id: 285,
        name: "Intellectual disability",
        version: "3.2",
      },
      {
        // Green panel for AAAS
        id: 145,
        name: "Congenital adrenal hypoplasia",
        version: "2.2",
      },
    ];
    const entities = await getEntities(panelsData);
    const entityNames = Object.keys(entities);
    const aaas = entities["AAAS"];

    expect(entityNames.length).toBe(1207);
    expect(aaas.panels.length).toBe(2);
    expect(aaas.panels[0].name).toBe("Congenital adrenal hypoplasia");
    expect(aaas.panels[1].name).toBe("Intellectual disability");
  });
});

describe("getPanelSubpanels()", () => {
  test("with superpanel", () => {
    const panelData = {
      entities: [
        {
          entity_name: "AAA1",
          panel: {
            id: 100,
            name: "Inherited disorder",
            version: "3.4",
          },
        },
      ],
    };

    const panelSubpanels = getPanelSubpanels(panelData);
    expect(panelSubpanels).toEqual({
      "Inherited disorder": {
        id: 100,
        version: "3.4",
      },
    });
  });

  test("without superpanel", () => {
    const panelData = {
      entities: [
        {
          entity_name: "AAA1",
        },
      ],
    };

    const panelSubpanels = getPanelSubpanels(panelData);
    expect(panelSubpanels).toEqual({});
  });
});
