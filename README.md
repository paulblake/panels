# GMS Signed Off Panels Static Site

Static site archive for GMS Signed Off panels.

## Overview

The app is built with GatsbyJS + TypeScript. It uses JestJS for testing and TailwindCSS for styling.

The available pages are:

- `/`

(defined in src/pages)

and:

- `/panels`
- `/panels/<panel_id>/v<panel_version>`

(defined in gatsby-node.js)

## Instructions

To install the dependencies:

```
yarn install
```

To run the tests:

```
yarn test
```

To run the development server:

```
yarn run develop
```

To build and serve the production website:

```
yarn run build
yarn run serve
```
