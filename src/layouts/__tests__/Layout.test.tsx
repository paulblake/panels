import React from "react";
import { create } from "react-test-renderer";

import Layout from "../Layout";

test("<Layout />", () => {
  const layout = create(
    <Layout>
      <div>Content.</div>
    </Layout>
  );
  expect(layout.toJSON()).toMatchSnapshot();
});
