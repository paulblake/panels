//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";

import Layout from "../layouts/Layout";

export default function Homepage() {
  return (
    <Layout>
      <div className="mx-auto mt-24 w-1/2">
        <p className="mb-3 font-bold">
          Welcome to the Genomic Medicine Service signed-off panels archive
        </p>

        <p className="mb-3">
          Welcome to the resource to view the signed-off panels that relate to
          genomic tests listed in the{" "}
          {
            <a
              href="https://www.england.nhs.uk/publication/national-genomic-test-directories/"
              className="text-nhs-blue hover:underline"
            >
              NHS National Genomic Medicine Service Test Directory for rare and
              inherited disorders and cancer
            </a>
          }
          .
        </p>

        <p className="mb-3">
          The panels in this resource contain the list of green genes, short
          tandem repeats (STRs) and regions (copy number variants) that were
          agreed by the NHSE disease specialist test groups and will be reviewed
          by the Clinical Reference Group going forward. The signed off version
          number is shown for each panel.
        </p>

        <p className="mb-3 font-bold">Searching</p>

        <p className="mb-3">
          Panels can be searched for by panel name or R code (e.g. R27). Search
          by gene or entity will show all the GMS signed-off panels that the
          gene or entity is on.
        </p>

        <p className="mb-3 font-bold">
          Latest versions of panels and leaving reviews
        </p>

        <p className="mb-3">
          The main Genomics England{" "}
          {
            <a
              href="https://panelapp.genomicsengland.co.uk/"
              className="text-nhs-blue hover:underline"
            >
              PanelApp
            </a>
          }{" "}
          knowledgebase shows the latest versions of the panels which have been
          updated by Genomics England curators. The latest version will not yet
          have been approved for use in GMS analysis.
        </p>

        <p className="mb-3">
          If you wish to add a review or suggest additional genes or entities
          for a panel please do this in the main Genomics England PanelApp
          knowledgebase.
        </p>

        <p className="mb-3 font-bold">Questions about the Test Directory</p>

        <p className="mb-10">
          For queries about tests within the Test Directory please refer to the
          following{" "}
          {
            <a
              href="https://www.england.nhs.uk/genomics/the-national-genomic-test-directory/"
              className="text-nhs-blue hover:underline"
            >
              page
            </a>
          }
          .
        </p>
      </div>
    </Layout>
  );
}
