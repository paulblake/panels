import React from "react";
import { create } from "react-test-renderer";

import Page404 from "../404";

test("<Page404 />", () => {
  const page404 = create(<Page404 />);
  expect(page404.toJSON()).toMatchSnapshot();
});
