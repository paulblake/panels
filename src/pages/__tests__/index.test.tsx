import React from "react";
import { create } from "react-test-renderer";

import HomePage from "../";

test("<HomePage />", () => {
  const homepage = create(<HomePage />);
  expect(homepage.toJSON()).toMatchSnapshot();
});
