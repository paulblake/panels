import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import EntitiesTemplate from "../entities-template";
import entities from "../../fixtures/all-entities.json";

function setup() {
  const entitiesTemplate = render(
    <EntitiesTemplate pageContext={{ entities }} />
  );

  return { entitiesTemplate };
}

test("Search for non-existing entities", () => {
  const { entitiesTemplate } = setup();
  expect(entitiesTemplate.container).toMatchSnapshot();

  userEvent.type(screen.getByRole("textbox"), "atxnxxx");
  expect(entitiesTemplate.container).toMatchSnapshot();
});

describe("<EntitiesTemplate />", () => {
  test("Search for existing entities", () => {
    const { entitiesTemplate } = setup();
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "atxn");
    expect(entitiesTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing entities", () => {
    const { entitiesTemplate } = setup();
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "atxnxxx");
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test Genes checkbox", () => {
    const { entitiesTemplate } = setup();
    const genesCheckbox = screen.getByRole("checkbox", { name: "Genes" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(genesCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(genesCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test STRs checkbox", () => {
    const { entitiesTemplate } = setup();
    const strsCheckbox = screen.getByRole("checkbox", { name: "STRs" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(strsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(strsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
  test("Test Regions checkbox", () => {
    const { entitiesTemplate } = setup();
    const regionsCheckbox = screen.getByRole("checkbox", { name: "Regions" });
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(regionsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();

    userEvent.click(regionsCheckbox);
    expect(entitiesTemplate.container).toMatchSnapshot();
  });
});
