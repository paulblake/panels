import React from "react";
import { create } from "react-test-renderer";

import PanelTemplate from "../panel-template";
import panel from "../../fixtures/panel-1.json";
import entities from "../../fixtures/entities.json";

test("<PanelTemplate />", () => {
  const panelTemplate = create(
    <PanelTemplate pageContext={{ panel, entities, versions: [] }} />
  );
  expect(panelTemplate.toJSON()).toMatchSnapshot();
});
