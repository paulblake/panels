import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import toJson from "enzyme-to-json";

import EntityTemplate from "../entity-template";
import EntityTable from "../../components/EntityTable";
import entity from "../../fixtures/entity-gene.json";

configure({ adapter: new Adapter() });

test("<EntityTemplate />", () => {
  const rootWrapper = shallow(<EntityTemplate pageContext={{ entity }} />);
  const entityTableWrapper = rootWrapper.find(EntityTable).at(0);
  expect(toJson(rootWrapper)).toMatchSnapshot();

  // Search for existing panels
  entityTableWrapper.props()["onSearchBarChange"]({
    target: {
      value: "disorder",
    },
  });
  expect(toJson(rootWrapper)).toMatchSnapshot();

  // Search for non-existing panels
  entityTableWrapper.props()["onSearchBarChange"]({
    target: {
      value: "disorderxxx",
    },
  });
  expect(toJson(rootWrapper)).toMatchSnapshot();
});
