import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import PanelsTemplate from "../panels-template";
import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";

describe("<PanelsTemplate />", () => {
  test("Search for existing name", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "intellectual");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing name", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "intellectualxxx");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for existing R-number", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "r10");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing R-number", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "r999");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for existing type", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "100k");
    expect(panelsTemplate.container).toMatchSnapshot();
  });

  test("Search for non-existing type", async () => {
    const panelsTemplate = render(
      <PanelsTemplate pageContext={{ panels, subpanels }} />
    );
    expect(panelsTemplate.container).toMatchSnapshot();

    userEvent.type(screen.getByRole("textbox"), "999k");
    expect(panelsTemplate.container).toMatchSnapshot();
  });
});
