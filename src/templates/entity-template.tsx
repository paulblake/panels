//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React, { ChangeEvent, useState } from "react";

import Layout from "../layouts/Layout";
import EntityHeader from "../components/EntityHeader";
import EntitySummary from "../interfaces/EntitySummary";
import EntityPanel from "../interfaces/EntityPanel";
import Subpanels from "../interfaces/Subpanels";
import EntityTable from "../components/EntityTable";

interface EntityTemplateProps {
  pageContext: {
    entity: {
      data: EntitySummary;
      panels: EntityPanel[];
    };
    subpanels: Subpanels;
  };
}

export default function EntityTemplate({
  pageContext: { entity, subpanels },
}: EntityTemplateProps) {
  const initialPanels = entity.panels;
  const [panels, setPanels] = useState(initialPanels);

  function onSearchBarChange(event: ChangeEvent<HTMLInputElement>) {
    const query = event.target.value.toLowerCase();

    const filteredPanels = initialPanels.filter(panel => {
      const panelName = panel.name.toLowerCase();

      return panelName.includes(query);
    });

    setPanels(filteredPanels);
  }

  return (
    <Layout>
      <div className="mt-6">
        <div>
          <EntityHeader entity={entity} />
          <div>
            <EntityTable
              panels={panels}
              subpanels={subpanels}
              onSearchBarChange={onSearchBarChange}
            />
          </div>
        </div>
      </div>
    </Layout>
  );
}
