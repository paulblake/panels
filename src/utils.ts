import { ConfidenceLevel } from "./interfaces/types";

/**
 * Determines whether a relevant disorder of a panel is an R-number.
 *
 * An R-number starts with a capital 'R' followed by a whole/decimal number Examples:
 * R41, R42.2
 *
 * @param {string} rd - Relevant disorder
 */
export function isRNumber(rd: string) {
  return /^R\d+(\.\d+)?$/.test(rd);
}

interface ConfiedenceLevelStylesMap {
  [level: string]: [name: string, style: string];
}

export function getConfidenceMetadata(confidenceLevel: ConfidenceLevel) {
  const styles: ConfiedenceLevelStylesMap = {
    "0": ["No list", "text-black bg-nhs-mid-grey border-nhs-dark-grey"],
    "1": ["Red", "text-white bg-nhs-red border-nhs-dark-red"],
    "2": ["Amber", "text-white bg-nhs-warm-yellow border-nhs-orange"],
    "3": ["Green", "text-white bg-nhs-green border-nhs-dark-green"],
  };

  return styles[confidenceLevel] || styles["0"];
}
