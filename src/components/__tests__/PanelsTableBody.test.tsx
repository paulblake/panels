import React from "react";
import { create } from "react-test-renderer";

import PanelsTableBody from "../PanelsTableBody";
import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";

test("<PanelsTableBody />", () => {
  const panelsTableBody = create(
    <PanelsTableBody panels={panels} subpanels={subpanels} />
  );
  expect(panelsTableBody.toJSON()).toMatchSnapshot();
});
