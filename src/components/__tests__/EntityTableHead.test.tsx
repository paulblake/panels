import React from "react";
import { create } from "react-test-renderer";

import EntityTableHead from "../EntityTableHead";
import entity from "../../fixtures/entity-gene.json";

describe("<EntityTableHead />", () => {
  test("with 1 panel", () => {
    const panels = [entity.panels[0]];
    const entityTableHead = create(
      <EntityTableHead panels={panels} onSearchBarChange={() => {}} />
    );
    expect(entityTableHead.toJSON()).toMatchSnapshot();
  });

  test("with more than 1 panel", () => {
    const panels = entity.panels;
    const entityTableHead = create(
      <EntityTableHead panels={panels} onSearchBarChange={() => {}} />
    );
    expect(entityTableHead.toJSON()).toMatchSnapshot();
  });
});
