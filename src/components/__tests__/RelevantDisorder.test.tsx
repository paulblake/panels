import React from "react";
import { create } from "react-test-renderer";

import RelevantDisorder from "../RelevantDisorder";

describe("<RelevantDisorder />", () => {
  test("with R-number", () => {
    const rd = "R42";
    const relevantDisorder = create(<RelevantDisorder rd={rd} />);
    expect(relevantDisorder.toJSON()).toMatchSnapshot();
  });

  test("without R-number", () => {
    const rd = "Cardiac arrythmias";
    const relevantDisorder = create(<RelevantDisorder rd={rd} />);
    expect(relevantDisorder.toJSON()).toMatchSnapshot();
  });
});
