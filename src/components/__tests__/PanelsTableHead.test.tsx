import React from "react";
import { create } from "react-test-renderer";

import PanelsTableHead from "../PanelsTableHead";
import panels from "../../fixtures/panels.json";

describe("<PanelsTableHead />", () => {
  test("with 1 panel", () => {
    const panelsTableHead = create(
      <PanelsTableHead panels={[panels[0]]} onSearchBarChange={() => {}} />
    );
    expect(panelsTableHead.toJSON()).toMatchSnapshot();
  });
  test("with more than 1 panel", () => {
    const panelsTableHead = create(
      <PanelsTableHead panels={panels} onSearchBarChange={() => {}} />
    );
    expect(panelsTableHead.toJSON()).toMatchSnapshot();
  });
});
