import React from "react";
import { create } from "react-test-renderer";

import PanelsTable from "../PanelsTable";
import panels from "../../fixtures/panels.json";
import subpanels from "../../fixtures/subpanels.json";

test("<PanelsTable />", () => {
  const panelsTable = create(
    <PanelsTable
      panels={panels}
      subpanels={subpanels}
      onSearchBarChange={() => {}}
    />
  );
  expect(panelsTable.toJSON()).toMatchSnapshot();
});
