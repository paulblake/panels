import React from "react";
import { create } from "react-test-renderer";

import EntityTable from "../EntityTable";
import entity from "../../fixtures/entity-gene.json";
import subpanels from "../../fixtures/subpanels.json";

test("<EntityTable />", () => {
  const { panels } = entity;
  const entityTable = create(
    <EntityTable
      panels={panels}
      subpanels={subpanels}
      onSearchBarChange={() => {}}
    />
  );
  expect(entityTable.toJSON()).toMatchSnapshot();
});
