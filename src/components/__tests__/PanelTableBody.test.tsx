import React from "react";
import { create } from "react-test-renderer";

import PanelTableBody from "../PanelTableBody";
import entities from "../../fixtures/entities.json";

test("<PanelTableBody />", () => {
  const panelTableBody = create(<PanelTableBody entities={entities} />);
  expect(panelTableBody.toJSON()).toMatchSnapshot();
});
