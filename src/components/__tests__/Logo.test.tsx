import React from "react";
import { create } from "react-test-renderer";

import Logo from "../Logo";

test("<Logo />", () => {
  const logo = create(<Logo />);
  expect(logo.toJSON()).toMatchSnapshot();
});
