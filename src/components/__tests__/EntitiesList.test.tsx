import React from "react";
import { create } from "react-test-renderer";

import EntitiesList from "../EntitiesList";
import entities from "../../fixtures/all-entities.json";

test("<EntitiesList />", () => {
  const entitiesList = create(<EntitiesList entities={entities} />);
  expect(entitiesList.toJSON()).toMatchSnapshot();
});
