import React from "react";
import { create } from "react-test-renderer";

import EntityTableBody from "../EntityTableBody";
import entity from "../../fixtures/entity-gene.json";
import subpanels from "../../fixtures/subpanels.json";

test("<EntityTableBody />", () => {
  const { panels } = entity;
  const entityTableBody = create(
    <EntityTableBody panels={panels} subpanels={subpanels} />
  );
  expect(entityTableBody.toJSON()).toMatchSnapshot();
});
