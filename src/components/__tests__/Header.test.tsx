import React from "react";
import { create } from "react-test-renderer";

import Header from "../Header";

test("<Header />", () => {
  const header = create(<Header />);
  expect(header.toJSON()).toMatchSnapshot();
});
