import React from "react";
import { create } from "react-test-renderer";

import EntitiesSearchBox from "../EntitiesSearchBox";
import entities from "../../fixtures/all-entities.json";

test("<EntitiesSearchBox />", () => {
  const entitiesSearchBox = create(
    <EntitiesSearchBox entities={entities} onSearchBarChange={() => {}} />
  );
  expect(entitiesSearchBox.toJSON()).toMatchSnapshot();
});
