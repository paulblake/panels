import React from "react";
import { create } from "react-test-renderer";
import { render, fireEvent } from "@testing-library/react";
import { navigate } from "gatsby";

import PanelHeader from "../PanelHeader";
import panel1 from "../../fixtures/panel-1.json";
import panel2 from "../../fixtures/panel-2.json";
import panel3 from "../../fixtures/panel-3.json";
import entities from "../../fixtures/entities.json";

jest.mock("gatsby", () => {
  const gatsby = jest.requireActual("gatsby");

  return {
    ...gatsby,
    navigate: jest.fn(),
  };
});

describe("<PanelHeader />", () => {
  test("with relevant disorders and types", () => {
    const panelHeader = create(
      <PanelHeader panel={panel1} entities={entities} versions={[]} />
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("without relevant disorders and types", () => {
    const panelHeader = create(
      <PanelHeader panel={panel2} entities={entities} versions={[]} />
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("with superpanel", () => {
    const panelHeader = create(
      <PanelHeader panel={panel3} entities={entities} versions={[]} />
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("without entities", () => {
    const panelHeader = create(
      <PanelHeader panel={panel1} entities={[]} versions={[]} />
    );
    expect(panelHeader.toJSON()).toMatchSnapshot();
  });
  test("navigates to another version", () => {
    navigate.mockReset();
    const { getByTestId } = render(
      <PanelHeader
        panel={panel1}
        entities={[]}
        versions={["3.2", "3.1", "3.0"]}
      />
    );
    fireEvent.change(getByTestId("selectVersion"), {
      target: { value: "3.0" },
    });
    expect(navigate.mock.calls).toMatchInlineSnapshot(`
      Array [
        Array [
          "/panels/123/v3.0",
        ],
      ]
    `);
  });

  test("old version includes warning banner", () => {
    const { getByText } = render(
      <PanelHeader
        panel={panel1}
        entities={[]}
        versions={["3.5", "3.2", "3.1", "3.0"]}
      />
    );
    expect(
      getByText("There is a more recent signed-off version of this panel")
    ).toBeInTheDocument();
  });
});
