//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";
import { Link } from "gatsby";
import { DateTime } from "luxon";

import PanelSummary from "../interfaces/PanelSummary";
import Subpanels from "../interfaces/Subpanels";
import RelevantDisorders from "./RelevantDisorders";

interface PanelsTableBodyProps {
  panels: PanelSummary[];
  subpanels: Subpanels;
}

export default function PanelsTableBody({
  panels,
  subpanels,
}: PanelsTableBodyProps) {
  return (
    <tbody>
      {panels.map(panel => (
        <tr key={`${panel.id}-v${panel.version}`}>
          <td className="border px-4 py-2">
            <div className="mb-2 text-xl font-semibold text-nhs-blue">
              <Link
                to={`/panels/${panel.id}/v${panel.version}`}
                className="hover:underline"
              >
                {panel.name}
              </Link>
            </div>
            {!!panel.relevant_disorders.length && (
              <div>
                Relevant disorders:{" "}
                <RelevantDisorders rds={panel.relevant_disorders} />
              </div>
            )}
            <div className="mb-2">Version {panel.version}</div>
            {!!panel.types.length && (
              <div className="mb-2">
                Panel Types: {panel.types.map(type => type.name).join(", ")}
              </div>
            )}
            {subpanels[panel.name] && (
              <div className="mb-2">
                Component of the following Super Panels:{" "}
                {subpanels[panel.name].superpanels.join(", ")}
              </div>
            )}
            <div className="font-semibold">
              Signed Off on{" "}
              {DateTime.fromISO(panel.signed_off, {
                locale: "en-GB",
              }).toLocaleString(DateTime.DATE_MED)}
            </div>
          </td>
          <td className="border px-4 py-2">{panel.stats.number_of_genes}</td>
        </tr>
      ))}
    </tbody>
  );
}
