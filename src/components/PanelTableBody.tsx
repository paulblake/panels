//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React from "react";
import { Link } from "gatsby";

import Entity from "../interfaces/Entity";
import { getConfidenceMetadata } from "../utils";

interface PanelTableBodyProps {
  entities: Entity[];
}

interface PathogenicityModes {
  [pm: string]: string;
}

// User-friendly names for some of the modes of pathogenicity
const pathogenicityModes: PathogenicityModes = {
  "Other - please provide details in the comments": "Other",
  "Loss-of-function variants (as defined in pop up message) DO NOT cause this phenotype - please provide details in the comments":
    "Loss-of-function variants DO NOT cause this phenotype",
};

export default function PanelTableBody({ entities }: PanelTableBodyProps) {
  return (
    <tbody className="font-light">
      {entities.map(entity => {
        const [name, styles] = getConfidenceMetadata(entity.confidence_level);

        return (
          <tr key={entity.entity_name}>
            <td className="border px-4 py-2 text-white font-semibold">
              <div className={`text-sm border rounded px-8 ${styles}`}>
                {name}
              </div>
            </td>
            <td className="border px-4 py-2">
              <div className="font-bold">
                <Link
                  to={`/entities/${entity.entity_name}`}
                  className="text-nhs-blue hover:underline"
                >
                  {entity.entity_name}
                </Link>
              </div>
              {entity.panel && (
                <div className="text-xs">{entity.panel.name}</div>
              )}
              {entity.entity_type === "str" && <span>STR</span>}
              {entity.entity_type === "region" && (
                <div className="text-xs">
                  <div>{entity.verbose_name}</div>
                  <div>{entity.entity_type}</div>
                </div>
              )}
            </td>
            <td className="border px-4 py-2">
              {entity.mode_of_inheritance || "N/A"}
            </td>
            <td className="border px-4 py-2">
              {entity.mode_of_pathogenicity
                ? // Return a user-friendly form of the mode of pathogenicity if there is one
                  pathogenicityModes[entity.mode_of_pathogenicity.trim()] ||
                  entity.mode_of_pathogenicity
                : "N/A"}
            </td>
            <td className="border px-4 py-2">
              {entity.tags.length ? entity.tags.join(", ") : "N/A"}
            </td>
          </tr>
        );
      })}
    </tbody>
  );
}
