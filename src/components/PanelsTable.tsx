//
// Copyright (c) 2020 Genomics England Ltd.
//
// This file is part of PanelApp
// (see https://panelapp.genomicsengland.co.uk).
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//
import React, { ChangeEvent } from "react";

import PanelSummary from "../interfaces/PanelSummary";
import Subpanels from "../interfaces/Subpanels";
import PanelsTableHead from "./PanelsTableHead";
import PanelsTableBody from "./PanelsTableBody";

interface OnSearchBarChange {
  (event: ChangeEvent<HTMLInputElement>): void;
}

interface PanelsTableProps {
  panels: PanelSummary[];
  subpanels: Subpanels;
  searchField: React.MutableRefObject<null>;
  onSearchBarChange: OnSearchBarChange;
}

export default function PanelsTable({
  panels,
  subpanels,
  searchField,
  onSearchBarChange,
}: PanelsTableProps) {
  return (
    <table className="border w-full">
      <colgroup>
        <col span="1" className="w-4/5" />
        <col span="1" className="w-1/5" />
      </colgroup>
      <PanelsTableHead
        panels={panels}
        searchField={searchField}
        onSearchBarChange={onSearchBarChange}
      />
      <PanelsTableBody panels={panels} subpanels={subpanels} />
    </table>
  );
}
