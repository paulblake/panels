const fs = require("fs");

const axios = require("axios");
const orderBy = require("lodash/orderBy");

const { PANELAPP_HOSTNAME = "panelapp.genomicsengland.co.uk" } = process.env;

async function getPanelsData(reporter, latest = true) {
  const startUrl = `https://${PANELAPP_HOSTNAME}/api/v1/panels/signedoff/${
    latest ? "" : "?display=all"
  }`;
  reporter.info(`Fetching: ${startUrl}`);

  let { data } = await axios.get(startUrl);
  let panelSummaries = [...data.results];

  while (data.next) {
    reporter.info(`Fetching: ${data.next}`);
    ({ data } = await axios.get(data.next));
    panelSummaries = panelSummaries.concat(data.results);
  }

  const orderedPanels = orderBy(
    panelSummaries.map(panelSummary => ({
      id: panelSummary.id,
      name: panelSummary.name,
      version: panelSummary.version,
      signed_off: panelSummary.signed_off,
      relevant_disorders: panelSummary.relevant_disorders,
      types: panelSummary.types.map(type => ({ name: type.name })),
      stats: {
        number_of_genes: panelSummary.stats.number_of_genes,
      },
    })),
    [panel => panel.name.toLowerCase()]
  );

  return orderedPanels;
}

/**
 * Helper function that returns the data of a specific panel
 *
 * @param {number} id - The panel ID
 * @param {string} version - The panel version
 */
async function getPanelData(id, version) {
  if (fs.existsSync(`./data/panels/${id}/v${version}.json`)) {
    return require(`./data/panels/${id}/v${version}.json`);
  }

  const url = `https://${PANELAPP_HOSTNAME}/api/v1/panels/${id}/?version=${version}`;
  const { data } = await axios.get(url);

  const entities = [...data.genes, ...data.strs, ...data.regions];
  const sortedEntities = orderBy(
    entities,
    [
      // Sort first by confidence level, then by name
      entity => entity.confidence_level,
      entity => entity.entity_name.toLowerCase(),
    ],
    ["desc", "asc"]
  );

  const panelData = {
    panel: {
      id: data.id,
      name: data.name,
      version: data.version,
      relevant_disorders: data.relevant_disorders,
      signed_off: data.signed_off,
      types: data.types.map(type => ({ name: type.name })),
    },
    entities: sortedEntities
      .map(entity => ({
        entity_name: entity.entity_name,
        entity_type: entity.entity_type,
        tags: entity.tags,
        verbose_name: entity.verbose_name,
        mode_of_pathogenicity: entity.mode_of_pathogenicity,
        gene_data: entity.gene_data
          ? {
              gene_name: entity.gene_data.gene_name,
              omim_gene: entity.gene_data.omim_gene,
            }
          : null,

        // Panel-specific information
        panel: entity.panel
          ? {
              id: entity.panel.id,
              name: entity.panel.name,
              version: entity.panel.version,
            }
          : null,
        confidence_level: entity.confidence_level,
        mode_of_inheritance: entity.mode_of_inheritance,
        phenotypes: entity.phenotypes,
      }))
      .filter(entity => Number(entity.confidence_level) >= 3),
  };

  fs.mkdirSync(`./data/panels/${id}`, { recursive: true });
  fs.writeFileSync(
    `./data/panels/${id}/v${version}.json`,
    JSON.stringify(panelData)
  );
  return panelData;
}

function getEntityPanel(panelData, panelEntity) {
  return {
    id: panelData.panel.id,
    name: panelData.panel.name,
    version: panelData.panel.version,
    relevant_disorders: panelData.panel.relevant_disorders,
    entity: {
      confidence_level: panelEntity.confidence_level,
      mode_of_inheritance: panelEntity.mode_of_inheritance,
      phenotypes: panelEntity.phenotypes,
    },
  };
}

function getEntitiesData(entities) {
  return {
    entities: orderBy(
      Object.keys(entities).map(entityName => ({
        entity_name: entities[entityName].data.entity_name,
        entity_type: entities[entityName].data.entity_type,
      })),
      [entity => entity.entity_name.toLowerCase()]
    ),
  };
}

async function getSubpanels(latestPanels) {
  const subpanels = {};

  for (const panelSummary of latestPanels) {
    const panelData = await getPanelData(panelSummary.id, panelSummary.version);

    // Only the entities of a superpanel have a 'panel' property
    const isSuperPanel =
      !!panelData.entities.length && !!panelData.entities[0].panel;

    // Add the subpanels of the superpanel to the 'subpanels' table
    if (isSuperPanel) {
      const superpanel = panelData.panel;

      for (const panelEntity of panelData.entities) {
        const subpanel = panelEntity.panel;

        if (!subpanels.hasOwnProperty(subpanel.name)) {
          subpanels[subpanel.name] = {
            superpanels: new Set(),
          };
        }
        subpanels[subpanel.name].superpanels.add(superpanel.name);
      }
    }
  }

  // Convert the superpanels property from a set to an array.
  // (Otherwise React will convert it to an empty object when we pass subpanels as a prop.)
  for (const subpanelName of Object.keys(subpanels)) {
    subpanels[subpanelName].superpanels = [
      ...subpanels[subpanelName].superpanels,
    ];
  }
  return subpanels;
}

async function getEntities(latestPanels) {
  const entities = {};
  const sortedEntities = {};

  for (const panelSummary of latestPanels) {
    const panelData = await getPanelData(panelSummary.id, panelSummary.version);

    // Only the entities of a superpanel have a 'panel' property
    const isSuperPanel =
      !!panelData.entities.length && !!panelData.entities[0].panel;

    // Add the panel entities to the 'entities' table
    // if the panel is not a superpanel
    if (!isSuperPanel) {
      for (const panelEntity of panelData.entities) {
        const entityPanel = getEntityPanel(panelData, panelEntity);

        if (!entities[panelEntity.entity_name]) {
          // Add the entity to 'entities' + add the panel to the entity
          entities[panelEntity.entity_name] = {
            data: {
              entity_name: panelEntity.entity_name,
              entity_type: panelEntity.entity_type,
              gene_data: panelEntity.gene_data,
              verbose_name: panelEntity.verbose_name,
            },
            panels: [entityPanel],
          };
        } else {
          // Only add the panel to the entity
          entities[panelEntity.entity_name].panels.push(entityPanel);
        }
      }
    }
  }

  for (const entityName of Object.keys(entities)) {
    sortedEntities[entityName] = {
      ...entities[entityName],
      panels: orderBy(
        entities[entityName].panels,
        [
          // Sort first by confidence level, then by name
          panel => panel.entity.confidence_level,
          panel => panel.name.toLowerCase(),
        ],
        ["desc", "asc"]
      ),
    };
  }
  return sortedEntities;
}

/**
 * Returns the subpanels of a panel.
 *
 * @param {*} panelData - The return value of getPanelData()
 * @param {*} panels - The return value of getPanels()
 */
function getPanelSubpanels(panelData) {
  const panelSubpanels = {};

  // Only the entities of a superpanel have a 'panel' property
  const isSuperPanel =
    !!panelData.entities.length && !!panelData.entities[0].panel;

  if (isSuperPanel) {
    for (const entity of panelData.entities) {
      const subpanel = entity.panel;
      if (!panelSubpanels.hasOwnProperty(subpanel.name)) {
        panelSubpanels[subpanel.name] = {
          id: subpanel.id,
          version: subpanel.version,
        };
      }
    }
  }
  return panelSubpanels;
}

module.exports = {
  getEntities,
  getEntitiesData,
  getEntityPanel,
  getPanelData,
  getPanelsData,
  getSubpanels,
  getPanelSubpanels,
};
