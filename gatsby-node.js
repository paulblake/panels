const {
  getEntities,
  getEntitiesData,
  getPanelData,
  getPanelsData,
  getSubpanels,
  getPanelSubpanels,
} = require("./utils");

// Special function handled by Gatsby
async function createPages({ reporter, actions: { createPage } }) {
  const latestPanels = await getPanelsData(reporter);
  const latestSubpanels = await getSubpanels(latestPanels);

  // Generate page with all the panels
  createPage({
    path: "/panels",
    component: require.resolve("./src/templates/panels-template.tsx"),
    context: {
      panels: latestPanels,
      subpanels: latestSubpanels,
    },
  });

  const latestPanelsIdVersionMap = {};
  latestPanels.forEach(panel => {
    latestPanelsIdVersionMap[panel.id] = panel.version;
  });

  const allPanels = await getPanelsData(reporter, false);
  const historicalPanels = allPanels.filter(
    panel => latestPanelsIdVersionMap[panel.id] !== panel.version
  );

  const allPanelsMap = {}; // {panelid: [panelVersion]}
  [...latestPanels, ...historicalPanels].forEach(panel => {
    if (!allPanelsMap.hasOwnProperty(panel.id)) {
      allPanelsMap[panel.id] = [];
    }
    allPanelsMap[panel.id].push(panel.version);
  });

  /**
   * link panels together so we can render dropdown menu to navigate between panels
   */

  // Generate pages for latest panels
  for (const panelSummary of latestPanels) {
    const panelData = await getPanelData(panelSummary.id, panelSummary.version);
    const panelSubpanels = getPanelSubpanels(panelData);
    panelData.panel.subpanels = panelSubpanels;
    panelData.versions = allPanelsMap[panelSummary.id];

    createPage({
      path: `/panels/${panelSummary.id}/v${panelSummary.version}`,
      component: require.resolve("./src/templates/panel-template.tsx"),
      context: panelData,
    });
  }

  // Generate pages for historical panels
  for (const panelSummary of historicalPanels) {
    const panelData = await getPanelData(panelSummary.id, panelSummary.version);
    const panelSubpanels = getPanelSubpanels(panelData);
    panelData.panel.subpanels = panelSubpanels;
    panelData.versions = allPanelsMap[panelSummary.id];

    createPage({
      path: `/panels/${panelSummary.id}/v${panelSummary.version}`,
      component: require.resolve("./src/templates/panel-template.tsx"),
      context: panelData,
    });
  }

  const entities = await getEntities(latestPanels);
  const entitiesData = getEntitiesData(entities);

  // Generate page for all entities
  createPage({
    path: "/entities",
    component: require.resolve("./src/templates/entities-template.tsx"),
    context: entitiesData,
  });

  // Generate pages for individual entities
  for (const entityName of Object.keys(entities)) {
    const entityData = entities[entityName];
    createPage({
      path: `/entities/${entityName}`,
      component: require.resolve("./src/templates/entity-template.tsx"),
      context: { entity: entityData, subpanels: latestSubpanels },
    });
  }
}

module.exports = {
  createPages,
};
